document.querySelector('#temapagina').addEventListener('click',  () => {
    let body= document.querySelector('body');
    let light= document.querySelector('.light-mode');
    let dark= document.querySelector('.dark-mode');
    if (body.dataset.theme==="dark"){
        body.dataset.theme= "light";
        light.classList.remove('hidden');
        dark.classList.add('hidden');

    }
    else {
        body.dataset.theme = "dark";
        dark.classList.remove('hidden');
        light.classList.add('hidden');

    }
});