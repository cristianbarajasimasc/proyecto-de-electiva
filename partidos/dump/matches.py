"""Fake data for pages"""
from datetime import datetime
MATCHES = [
    [
        {
            "week": 29,
            "date": datetime(2024, 3, 16),
            "team1": {
                "name": "Mallorca",
                "logo": "mallorca.png",
                "score": "1"
            },
            "team2": {
                "name": "Granada",
                "logo": "granada.png",
                "score": "0"
            },
            "stadium": "Son Moix",
            "channel": "Movistar LaLiga"
        },
        {
            "week": 29,
            "date": datetime(2024, 3, 17,9,0),
            "team1": {
                "name": "Sevilla",
                "logo": "sevilla.png",
                "score": "1"
            },
            "team2": {
                "name": "Celta Vigo",
                "logo": "celta-de-vigo.png",
                "score": "2"
            },
            "stadium": "Ramón Sánchez-Pizjuán",
            "channel": "Movistar LaLiga"
        },
        {
            "week": 29,
            "date": datetime(2024, 3, 17,11,15),
            "team1": {
                "name": "Villarreal",
                "logo": "villareal.png",
                "score": "1"
            },
            "team2": {
                "name": "Valencia",
                "logo": "valencia.png",
                "score": "0"
            },
            "stadium": "Estadio de la Cerámica",
            "channel": "Movistar LaLiga"
        },
        {
            "week": 29,
            "date": datetime(2024,3,17),
            "team1": {
                "name": "U.D. Las Palmas",
                "logo": "las-palmas.png",
                "score": 0
            },
            "team2": {
                "name": "Almería",
                "logo": "almeria.png",
                "score": 1
            },
            "stadium": "Gran Canaria",
            "channel": "Movistar LaLiga"
        }
    ],
    [
        {
            "week": 30,
            "date": datetime(2024,3,31,15,0),
            "team1": {
                "name": "Real Madrid",
                "logo": "real-madrid.png",
                "score": ""
            },
            "team2": {
                "name": "Athletic",
                "logo": "athletic-bilbao.png",
                "score": ""
            },
            "stadium": "Santiago Bernabeu",
            "channel": "Movistar LaLiga"
        }
    ]
]
