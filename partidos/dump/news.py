NEWS = {
    "carousel": [
    {
        "tag": "EQUIPOS",
        "date":"MAR 19.03.2024",
        "title":"José Ramón Sandoval, nuevo entrenador del Granada CF",
        "desc":"El conjunto nazarí ha anunciado la llegada de José Ramón Sandoval al banquillo del primer equipo, sustituyendo a Alexander Medina en el cargo.",
        "img": "carousel1.jpeg",
    }
    ,
    {
        "tag": "PARTIDOS",
        "date":"MAR 19.03.2024",
        "title":"Vini Jr. continúa en modo imparable",
        "desc":"El futbolista brasileño del Real Madrid volvió a marcar dos goles en el triunfo de su equipo por 2-4 ante CA Osasuna y sigue en racha de cara al gol en los últimos partidos.",
        "img": "carousel2.jpeg",
    }
    ,
    {
        "tag": "PARTIDOS",
        "date":"MAR 17.03.2024",
        "title":"Barcelona llega al segundo lugar de La Liga Española tras vencer al Atlético de Madrid",
        "desc":"El FC Barcelona cierra la jornada con un 0-3 ante el Atlético, asaltando al equipo dirigido por Diego Simeone para ubicarse segundos en la Liga con 64 puntos y se colocan ahora a ocho puntos del Madrid. ",
        "img": "carousel3.jpg",
    }
    ],
    "section":[
        {
            "title":"Real Madrid denuncia a árbitro de LaLiga a raíz de insultos racistas contra Vinicius Jr",
            "img": "section1.jpeg",
            "date":"18.03.2024",
        },
        {
            "title":"Courtois sufre nueva lesión de rodilla y se pierde el resto de la temporada",
            "img": "section2.jpeg",
            "date":"19.03.2024",
        },
        {
            "title":"Asi quedara la clasificación de LALIGA según la inteligencia artificial",
            "img": "section3.jpeg",
            "date":"18.03.2024",
        },
        {
            "title":"""Xavi invita a la hinchada del Barcelona a "soñar" con LaLiga""",
            "img": "section4.jpeg",
            "date":"18.03.2024",
        },
        {
            "title":"Iñaki Williams lidera el ranking de disparos a puerta de LaLiga",
            "img": "section5.jpeg",
            "date":"18.03.2024",
        },
        {
            "title":"Almería y César Montes consiguen su primera victoria en LaLiga 2023-24",
            "img": "section6.jpeg",
            "date":"17.03.2024",
        },
        {
            "title":"El Betis no perdía tres veces seguidas en LaLiga desde 2020",
            "img": "section7.jpeg",
            "date":"18.03.2024",
        },
    ]
    } 