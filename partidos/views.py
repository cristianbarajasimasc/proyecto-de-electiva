from django.shortcuts import render
from .dump import MATCHES,TABLE, NEWS


# Create your views here.
def index(request):
    return render(request, 'partidos/pages/index/index.html', {'news': NEWS})

def matches(request):
    """Show upcoming matches"""


    return render(request,'partidos/pages/upcoming-matches.html', {'matches': MATCHES})

def table(request):
    return render(request, 'partidos/pages/table.html', {'teams': TABLE})
