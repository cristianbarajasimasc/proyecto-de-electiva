"""Custom template tags for date formatting"""
from datetime import datetime
import locale
from django.template import Library,defaultfilters
register = Library()


@register.filter
def timesince_plus(value):
    """Filter - returns string with humanized time difference"""
    locale.setlocale(locale.LC_TIME, "es_ES.UTF-8")
    if value.date() > datetime.now().date():
        return "Programado para el " + \
            value.strftime("%d %B %Y") + \
            " a las " + value.strftime("%H:%M")

    return "hace "+ defaultfilters.timesince(value)
